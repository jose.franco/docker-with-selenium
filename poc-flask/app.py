from flask import Flask, request, Response ,jsonify
import time
from flask_caching import Cache


app = Flask(__name__)

config = {
    "DEBUG": True,          # some Flask specific configs
    "CACHE_TYPE": "simple", # Flask-Caching related configs
    "CACHE_DEFAULT_TIMEOUT": 300
}
app.config.from_mapping(config)
cache = Cache(app)

@app.route("/")
@cache.cached(timeout=120)
def dolar():
    from Principal import Principal
    sel=Principal(2)
    sel.start()
    sel.join()
    return(jsonify(sel.resultado))



if __name__ == "__main__":
    #print(ls())
    app.run(debug=True, host='0.0.0.0', port=80)