from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
import threading
import time

class Principal(threading.Thread):
    def __init__(self,x):
        self.__x = x
        threading.Thread.__init__(self)
        self.resultado=None


    def run (self):  # run() se utiliza para definir el comportamiento del hilo
        a=self.getVista()
        self.resultado=a
        print(self.resultado)


    def getVista(self):
        options = Options()
        options.headless = True
        driver=webdriver.Firefox(options=options)
        flag=False
        while (flag==False):
            try:
                driver.get('http://www.dolarhoy.com/cotizacion-dolar#')
                flag=True
                time.sleep(2)
            except:
                pass      
        try:
            PromC=driver.find_element_by_css_selector(".col-md-8 > div:nth-child(2) > div:nth-child(1) > h4:nth-child(1) > span:nth-child(1)").text
            PromV=driver.find_element_by_css_selector(".col-md-8 > div:nth-child(2) > div:nth-child(2) > h4:nth-child(1) > span:nth-child(1)").text
            
            b1n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(1) > td:nth-child(1) > a").text
            b1c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(1) > td:nth-child(2)").text
            b1v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(1) > td:nth-child(3)").text
            
            b2n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(2) > td:nth-child(1) > a").text
            b2c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(2) > td:nth-child(2)").text
            b2v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(2) > td:nth-child(3)").text
            
            b3n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(3) > td:nth-child(1) > a").text
            b3c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(3) > td:nth-child(2)").text
            b3v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(3) > td:nth-child(3)").text

            b4n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(4) > td:nth-child(1) > a").text
            b4c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(4) > td:nth-child(2)").text
            b4v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(4) > td:nth-child(3)").text
            
            b5n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(5) > td:nth-child(1) > a").text
            b5c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(5) > td:nth-child(2)").text
            b5v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(5) > td:nth-child(3)").text
            
            b6n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(6) > td:nth-child(1) > a").text
            b6c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(6) > td:nth-child(2)").text
            b6v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(6) > td:nth-child(3)").text
            
            b7n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(7) > td:nth-child(1) > a").text
            b7c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(7) > td:nth-child(2)").text
            b7v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(13) > table > tbody > tr:nth-child(7) > td:nth-child(3)").text
            
            b8n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(14) > table > tbody > tr:nth-child(2) > td:nth-child(1) > a").text
            b8c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(14) > table > tbody > tr:nth-child(2) > td:nth-child(2)").text
            b8v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(14) > table > tbody > tr:nth-child(2) > td:nth-child(3)").text
            
            b9n=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(14) > table > tbody > tr:nth-child(3) > td:nth-child(1) > a").text
            b9c=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(14) > table > tbody > tr:nth-child(3) > td:nth-child(2)").text
            b9v=driver.find_element_by_css_selector("body > div > div > div > div.col-md-8 > div:nth-child(14) > table > tbody > tr:nth-child(3) > td:nth-child(3)").text


            driver.quit()
        except:
            driver.quit()
        respuesta={'Compra':PromC,'Venta':PromV,
        'Bancos':[
        {'Banco':b1n,'Compra':b1c,'Venta':b1v},
        {'Banco':b2n,'Compra':b2c,'Venta':b2v},
        {'Banco':b3n,'Compra':b3c,'Venta':b3v},
        {'Banco':b4n,'Compra':b4c,'Venta':b4v},
        {'Banco':b5n,'Compra':b5c,'Venta':b5v},
        {'Banco':b6n,'Compra':b6c,'Venta':b6v},
        {'Banco':b7n,'Compra':b7c,'Venta':b7v},
        {'Banco':b8n,'Compra':b8c,'Venta':b8v},
        {'Banco':b9n,'Compra':b9c,'Venta':b9v},
        ]
        }
        return (respuesta)
